import socket
import threading

class Server:
	"""
    Server class
    The server allows multiple TCP connections
    """
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	connections = []
	defaultHost = socket.gethostname()
	defaultPort = 0

	def __init__(self):
		"""
		Constructor
	    @param self
	    @return void
	    @comments the default host (IP) and PORT are displayed
	    """
		self.sock.bind((self.defaultHost, self.defaultPort))
		print(self.getServerID())
		self.sock.listen(5)

	def getServerID(self):
		"""
		Get the IP and Port of the server
	    @param self
	    @return tuple that contains the IP and the PORT of the server
	    """
		return self.sock.getsockname()

	def handleConnections(self, conn, addr):
		"""
		Handle the connections
	    @param conn connection accepted by the server
	    @param addr IP and PORT of the client
	    @return void
	    @comments this function receives and returns the messages 
	    to all the connected clients. If a client send "exit", it is 
	    going to be remove from the list of connections
	    """
		while True:
			data = conn.recv(1024)
			if not data or str(data, "utf-8") == "exit":
				conn.close()
				print(str(addr[0])+":"+str(addr[1])+" disconnected!")
				self.connections.remove(conn)
				break
			else:
				for connection in self.connections:
					connection.send(bytes(str(str(addr[0])+":"+str(addr[1])+" -> "+str(data, "utf-8")), "utf-8"))

	def run(self):
		"""
		Runing the server
	    @param self
	    @return void
	    @comments if there is an error accepting the connection
	    the loop will stop. Every connection is handled in a new thread
	    """
		while True:
			try:
				conn, addr = self.sock.accept()
			except OSError:
				break
			sThread = threading.Thread(target=self.handleConnections, args=(conn, addr))
			sThread.daemon = True
			sThread.start()
			self.connections.append(conn)
			print(str(addr[0])+":"+str(addr[1])+" connected!")
		self.close()

	def close(self):
		"""
		Close the socket of the server
	    @param self
	    @return void
	    """
		self.sock.close()