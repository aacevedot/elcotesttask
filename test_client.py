import unittest
import socket
import threading
from client import Client

class TestClient(unittest.TestCase):
	def run_test_server(self):
	    # Creating a test server using TCP
	    # If the client send "exit", the server is 
	    # going close this connection
	    test_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	    test_server.bind((socket.gethostname(), 7777))
	    test_server.listen(1)
	    conn, addr = test_server.accept()
	    while True:
	    	data = conn.recv(1024)
	    	if not data or str(data, "utf-8") == "exit":
	    		conn.close()
	    		break
	    	conn.send(data)
	    test_server.close()

	def test_client_connect_server(self):
		# Start the test server in a new thread
		server_thread = threading.Thread(target=self.run_test_server)
		server_thread.start()
		
		# Creating and conneting the test client
		test_address = (socket.gethostname(), 7777)
		test_address = ":".join(map(str, test_address))
		test_client = Client(test_address)
		test_client.connect(True)
		
		# Test 1: Sending&Receiving a normal string
		test_message = "test"
		test_client.sendMessage(test_message)
		data = test_client.receiveMessage()
		self.assertEqual(str(data, "utf-8"), test_message)

		# Test 2: Sending&Receiving large text
		test_message = "Lorem ipsum dolor."
		test_client.sendMessage(test_message)
		data = test_client.receiveMessage()
		self.assertEqual(str(data, "utf-8"), test_message)

		# Test 3: Sending&Receiving numbers
		test_message = 1000000
		test_client.sendMessage(test_message)
		data = test_client.receiveMessage()
		self.assertEqual(int(data), test_message)

		# Test 4: Quit from the server using "exit"
		test_message = "exit"
		test_client.sendMessage(test_message)
		data = test_client.receiveMessage()
		self.assertEqual(str(data, "utf-8"), "")

		# Test 5: Trying to send a message after closing the server
		with self.assertRaises(ConnectionAbortedError):
			test_message = "test"
			test_client.sendMessage(test_message)
			data = test_client.receiveMessage()

		# Disconnecting the client
		test_client.disconnect()

		# Make sure that the server's thread finished
		server_thread.join()

if __name__ == '__main__':
	unittest.main()