import socket
import threading

class Client:
	"""
    Client class
    Several clients can be connected to the same server
    """
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	shutdown = False
	remoteHost = ""
	remotePort = ""

	def __init__(self, address):
		"""
		Contructor
	    @param address string with the format IP:PORT
	    @return void
	    """
		self.remoteHost, self.remotePort = address.split(":")

	def sendMessage(self, msg):
		"""
		Send a message to the server
	    @param msg message to be send
	    @return status
	    @comments if the msg is not a string it will be converted
	    """
		if (type(msg) is not str):
			msg = str(msg)
		return self.sock.send(bytes(msg, "utf-8"))

	def handleSending(self):
		"""
		Handle the sending of the messages
	    @param self
	    @return void
	    @comments this function can be stopped by KeyboardInterrupt (ctrl+c).
	    Also, it's possible to finish the current connection by typing "q"
	    """
		msg = input("")
		while msg != "q":
			try:
				if msg != "":
					self.sendMessage(msg)
				msg = input("")
			except KeyboardInterrupt:
				break

	def receiveMessage(self):
		"""
		Receive a message from the server
	    @param self
	    @return void
	    @comments in this case the max size of the buffer is 1024 bytes
	    """
		return self.sock.recv(1024)

	def handleReception(self):
		"""
		Handle the reception of the messages
	    @param self
	    @return void
	    @comments the messages are decoded by using UTF-8
	    """
		while not self.shutdown:
			data = self.receiveMessage()
			if not data:
				break
			print(str(data, "utf-8"))

	def connect(self, env):
		"""
		Enable the connection with the server
	    @param env enviroment boolean (True = dev, False = prod)
	    @return void
	    @comments if env is True, just the connection will be initialized
	    """
		self.sock.connect((self.remoteHost, int(self.remotePort)))		

		if not env:
			print("Connected...")

			cThread = threading.Thread(target=self.handleReception)
			cThread.daemon = True
			cThread.start()

			self.handleSending()
			self.disconnect()

	def disconnect(self):
		"""
		Finalize the connection with the server
	    @param self
	    @return void
	    @comments a message "exit" is sent to the server if the connection
	    has not been aborted. This allows to the server remove this client
	    from the list of connections
	    """
		try:
			self.sock.send(bytes("exit", "utf-8"))
		except ConnectionAbortedError:
			pass
		self.shutdown = True
		self.sock.close()

	def getClientID(self):
		"""
		Get the IP and Port of a client
	    @param self
	    @return tuple that contains the IP and the PORT of the client
	    """
		return self.sock.getsockname()
		