import unittest
import socket
import threading
import time
from random import randint
from server import Server

class TestServer(unittest.TestCase):
	def test_server_tcp_clients(self):
		# Start the server in a new thread
		server = Server()
		server_ip, server_port = server.getServerID()
		server_thread = threading.Thread(target=server.run)
		server_thread.start()

		# This if just in case (if a client tries to 
		# connect to the server before the thread binds 
		# and listens)
		time.sleep(0.1)

		# Test 1: Connecting a single client
		test_client = socket.socket()
		test_client.settimeout(1)
		test_client.connect((server_ip, server_port))
		test_client.close()
		
		# Test 2: Connecting multiple clients
		test_clients = []
		total_clients = 10
		for x in range(0,total_clients):
			temp = socket.socket()
			temp.settimeout(1)
			temp.connect((server_ip, server_port))
			time.sleep(0.1)
			test_clients.append(temp)

		# Test 3: Sending a message from a random client
		# it should be returned by the server to each client 
		# with exactly the same structure/value for everyone
		who_send = randint(0, len(test_clients)-1)
		test_message = "test"
		test_clients[who_send].send(bytes(test_message, "utf-8"))
		check_server_reply = ":".join(map(str, test_clients[who_send].getsockname()))+" -> "+test_message

		# Finalizing the connections randomly
		while len(test_clients) > 0:
			closing_random_connection = randint(0, len(test_clients)-1)
			# Checking if every client received the same test message
			data = test_clients[closing_random_connection].recv(1024)
			self.assertEqual(str(data, "utf-8"), check_server_reply)
			time.sleep(0.1)
			test_clients[closing_random_connection].close()
			test_clients.remove(test_clients[closing_random_connection])

		# closing the server socket
		server.close()
		
		# Trying to connect after closing the server
		test_client = socket.socket()
		with self.assertRaises(ConnectionRefusedError):
			test_client.connect((server_ip, server_port))
		test_client.close()
		
		# Make sure that the server's thread finished
		server_thread.join()

if __name__ == '__main__':
	unittest.main()