import sys
import re

from server import Server
from client import Client

if __name__ == '__main__':
	if (len(sys.argv) > 2):
		remote_address = sys.argv[2]
		# Validation of the arguments (ip and port)
		# just to be sure that they're fine
		if re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d+$",remote_address):
			client = Client(remote_address)
			client.connect(False)
		else:
			print(remote_address + " -> doesn't have the format: IP:PORT. Please, try again!")
	else:
		server = Server()
		server.run()